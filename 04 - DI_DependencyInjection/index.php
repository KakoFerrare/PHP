<?php


class Conn{
    
    public function conect(){
        $conn = new \PDO("mysql:host=localhost", "root", "fcm020964");
        return $conn;
    }
    
}

class User{
    private $conn;
    //AQUI APLICO A INJEÇÃO DE DEPENDENCIA, DIGO QUE PRA CRIAR UM OBJETO DE USUÁRIO
    //EU PRECISO PASSAR UM OBJETO DE CONEXÃO
    public function __construct(\Conn $pdo) {
        //$conn = new \PDO("mysql:host=localhost", "root", "fcm020964");
        $this->conn = $pdo;
    }
}

class Cart{
    private $user;
    public function __construct(\User $user) {
        $this->user= $user;
    }
    
    public function getDados(){
        echo "dados";
    }
}

//CONTAINER DA CLASS DE CART, COMO PRA CRIAR UM OBJETO DE CART EU PRECISO DE UM USUARIO
//QUE POR SUA VEZ PRECISA DE UMA CONNEXÃO, AQUI EU DEFINI UM CONTAINER QUE JA CRIA TUDO ISSO PRA MIM
//EU APENAS CHAMO O OBJETO
class ContainerCart{
    public function __construct() {
        $con = new Conn;
        $user = new User($con);
        $cart = new Cart($user);
        return $cart->getDados();
    }
}
$cart = new ContainerCart();



$connec = new Conn();
$user = new User($connec);