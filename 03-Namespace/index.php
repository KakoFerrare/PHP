<?php
//DEI REQUIRE EM UM ARQUIVO PHP QUE CONTEM O REQUIRE DAS DUAS CLASSES CONTA DE NAMESPACE DIFERENTES
require_once './app/start.php';

//NAMESPACE AJUDA A NÃO DAR CONFLITO NAS CLASSES QUE CONTEM O MESMO NOME


//PRIMEIRA FORMA DE CHAMAR CLASSES IGUAIS
//$contaFinanceiro = new app\Libries\Financeiro\Conta;
//$contaRH = new app\Libries\RH\Conta;
//PRIMEIRA FORMA DE CHAMAR CLASSES IGUAIS


//MELHOR FORMA DE CHAMAR AS CLASSES DANDO USE E RENOMEANDO-AS
use Libries\Financeiro\Conta as ContaFinanceiro,
    Libries\Financeiro\Extrato;
//COMO EXTRATO E CONTA DO FINANCEIRO, TEM O MESMO NAMESPACE, APENAS PASSEI VIRGULA DEPOIS DO CAMINHO
//DO NAMESPACE E ADICIONEI O EXTRATO

use Libries\RH\Conta as ContaRH;
use Libries\Gerencia\validaSalario;


$contaFinanceiro = new ContaFinanceiro;
$contaRH = new ContaRH();
$extrato = new Extrato();

$contaFinanceiro->teste();
$contaRH->teste();
$extrato->getExtrato();


$salario = new validaSalario();
$salario->getSalario();
