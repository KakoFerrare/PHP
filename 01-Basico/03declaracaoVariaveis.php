<?php

//VARIAVEL ALOCA OU GUARDA UM VALOR NA MEMÓRIA
//BOAS PRÁTICAS: VARIAVEL NÃO PODE TER ACENTO OU CARACTERES ESPECIAIS
//NOME DERIVADO PARA VARIAVEL: $novaVariavel
$variavel = "Teste 1<br>";

$numeroInteiro = 12;
$numeroFlutuante = 12.4;
$tipoBoolean = true;

$array = [1,2,3,4,5,6,7];
//OU
$arrayNovo = array(1,2,3,4,5,6,7);

$nulo = null;

var_dump($nulo);
