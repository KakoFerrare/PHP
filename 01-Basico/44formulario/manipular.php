<?php

//FORMA DE PEGAR DADOS VIA GET
//echo $_GET["nome"]. " Abriu arquivo";


//FORMA DE PEGAR DADOS VIA POST
//echo $_POST["nome"]. " Abriu arquivo";

//REQUEST, INDEPENDENTE DO POST OU GET ELE CAPTURA, ELE É SÓ UM POUCO MAIS LENTO, POIS ELE
//VERIFICA TODA A SESSÃO
//echo $_REQUEST["nome"]. " Abriu arquivo";

if(isset($_POST["nome"]) && !empty($_POST["nome"])){
    echo "Seja bem-vindo ". $_POST["nome"];    
}else{
    echo "Nenhum usuário!";
}
