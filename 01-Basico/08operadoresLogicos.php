<?php
//SE OS DOIS LADOS FOREM VERDADEIROS ELE RETORNA TRUE
var_dump(1==1 AND 2==2);
var_dump(1==1 AND 2==5);
//SEMELHANTE E PREFERENCIAL
var_dump(1==1 && 2==5);

//SE AO MENOS UM LADO ESTIVER VERDADEIRO ELE RETORNA TRUE
var_dump(1==2 OR 2==2);
var_dump(1==1 OR 2==5);
//SEMELHANTE E PREFERENCIAL
var_dump(1==1 || 2==5);
//SEMELHANTE
var_dump(1==1 XOR 2==5);


//NEGAÇÃO

var_dump(!1==2);


