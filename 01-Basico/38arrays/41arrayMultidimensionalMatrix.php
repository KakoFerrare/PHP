<?php

$jogoVelha = [
    'linha1'=> ['0','x','0', 'x', '0', 'x'],
    'linha2'=> ['x','0','x', '0', 'x', '0'],
    'linha3'=> ['0','x','0', 'x', '0', 'x'],
    'linha4'=> ['x','0','x', '0', 'x', '0'],
];

var_dump($jogoVelha);
echo "<br><br>";

$jogoVelha = [
    'linha1'=> ['0','x','0', 'x', '0', 'x'],
    'linha2'=> ['x','0','x'],
    'linha3'=> ['0','x','0', 'x', '0', 'x'],
    'linha4'=> ['x'],
];

var_dump($jogoVelha);
echo "<br><br>";

$jogoVelha = [
    'linha1'=> ['0','x','0', 'x', '0', 'x'],
    ['x','0','x', '0', 'x', '0'],
    'linha3'=> ['0','x','0', 'x', '0', 'x'],
    ['x','0','x', '0', 'x', '0'],
];

var_dump($jogoVelha);
echo "<br><br>";


$jogoVelha = [
    'linha1'=> ['0','x'=>[1,2,3,4,5 => [4,3,2,1]],'0', 'x', '0', 'x'],
    ['x','0','x', '0', 'x', '0'],
    'linha3'=> ['0','x','0', 'x', '0', 'x'],
    ['x','0','x', '0', 'x', '0'],
];

var_dump($jogoVelha);
echo "<br><br>";


echo "foreach dentro de foreach<br>";
$jogoVelha = [
    'linha1'=> ['0','x','0', 'x', '0', 'x'],
    'linha2'=> ['x','0','x', '0', 'x', '0'],
    'linha3'=> ['0','x','0', 'x', '0', 'x'],
    'linha4'=> ['x','0','x', '0', 'x', '0'],
];

foreach ($jogoVelha as $jogo){
    foreach ($jogo as $posicao){
    echo $posicao."<br>";
    }
        
}

