<?php

$texto = "Minha string de caracteres";
//APARTIR DO TERCEIRO CARACTER
echo substr($texto, 3)."<br>";

//APENAS OS TRES ULTIMOS CARACTERES
echo substr($texto, -3)."<br>";

//COMEÇOU DO QUARTO E PEGOU MAIS 3 CARACATERES A FRENTE
echo substr($texto, 4, 3)."<br>";

//COMEÇOU DO SEXTO CARACTER E FOI ATÉ OS 3 ULTIMOS
echo substr($texto, 6, -3)."<br>";

