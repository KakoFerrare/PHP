<?php

/**
 * \n SEMELHANTE AO BR, PULA LINHA
 * \r
 * \t TABULAÇÃO
 * \\ BARRA INVERTIDA   
 * \" MOSTRA AS ASPAS
 * \$ MOSTRAR O DOLAR
 * 
 * 
 * 
 * 
 * 
 * 
 */

//MOSTRA AS ASPAS EM VOLTA DO TEXTO
echo "Um \"texto\" qualquer, para exibir as \n \r manipulações de strings<br>";

//DA QUEBRA DE LINHA, NÃO NO HTML, MAS EM UM DOC DE TEXTO
echo "Um texto qualquer, para exibir as \n \r manipulações de strings<br>";

//INTERPRETA COMO STRING
echo "Um \$texto qualquer, para exibir as manipulações de strings";
