<?php
//VERIFICA SE 1 É IGUAL A 1
var_dump(1==1);

//VERIFICA SE 1 É IGUAL A 1 E SE O TIPO É O MESMO
var_dump("1"===1);


//VERIFICA SE 1(4) É DIFERENTE DE 1
//OU USANDO <>
var_dump(1!=1);
var_dump(4!=1);
var_dump(4<>1);

//MAIOR OU MENOS
var_dump(4 > 3);
var_dump(4 < 3);
var_dump(4 <= 4);



