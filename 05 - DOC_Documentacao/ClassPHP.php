<?php
//FORMA DE DOCUMENTAR A CLASSE, ABAIXO

/**
 * Essa é uma classe que gera log personalizado no sistema
 * 
 * @author kako - <kakoferrare@hotmail.com>
 * @copyright (c) 2016, nome_empresa
 * @since 01/10/2015 comecei a classe em 2015
 * @version 1
 */
class ClassPHP {
    /**
     * Este log vem da classe testeLog do módulo TDD
     * 
     * @var String => Recebe o nome do log a ser gerado
     */
    public $teste;
    
    /**
     * Este log vem da classe testeLog do módulo TDD
     * 
     * @var String => Recebe o nome do log a ser gerado
     */
    public $teste2;
    
    
    /**
     * Esse é um método que faz a leitura dos logs do sistema, envia um e-mail
     * informando para o gestor, caso o sisteme esteja em risco
     * 
     * @author John Doe - <john.doe@example.com>
     * @since 01/02/2016
     * @version 1
     * @access public
     */
    public function metodoTeste(){
    }
    
    
    /**
     * Esse é um método que limpa os logs do sistema
     * 
     * @author John Doe - <john.doe@example.com>
     * @since 01/02/2016
     * @version 1
     * @access public
     * @param string $log => Este é uma string que identifica o log
     * @param int $log2 => Este é um inteiro que identifica o log
     */
    public function metodoTeste2($log, $log2){
    }
    
    
    /**
     * Esse é um método que limpa os logs do sistema
     * 
     * @author John Doe - <john.doe@example.com>
     * @since 01/02/2016
     * @version 1
     * @access public
     * @param string $log Este é uma string que identifica o log
     * @return inteiro => Ele retorna um inteiro para true ou false
     */
    public function metodoTeste3(){
        
        return 1;
        
    }
    
}

$teste = new ClassPHP();
