<?php

//CRIAR UM OBJETO DE FORMA DINAMICA, STDCLASS ME DEIXA CRIAR UM OBJETO QUALQUER
$obj = new stdClass();

$obj->nome = 'Kako';
$obj->telefone = 4353453;
$obj->cpf = 355465645;

foreach ($obj as $key => $value){
    echo "{$key} -> {$value} <br>";
}

$arrayName = array();
$arrayName['nome'] = 'Mateus';
$arrayName['telefone'] = 223434;
$arrayName['cpf'] = 4545454;

var_dump($arrayName);