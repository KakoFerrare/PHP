<?php
function showMsgError($erro, $msg, $file, $line){
    switch ($erro){
        case E_USER_ERROR: 
            $cor = 'red'; 
            break;
        case E_USER_NOTICE: 
            $cor = 'blue'; 
            break;
        case E_USER_WARNING: 
            $cor = 'yellow'; 
            break;
    }
    echo "<p style='color:{$cor}'>Erro na linha {$line} - {$erro} (No arquivo {$file}))</p>";
}
//O OUVINTE DO TRATAMENTO DE ERROS FICOU SETADO PARA O MÉTODO showMsgError
set_error_handler('showMsgError');

if(!file_exists('arquiv23o.txt')){
    trigger_error('O arquivo não existe', E_USER_NOTICE);
    trigger_error('O arquivo não existe', E_USER_WARNING);
    trigger_error('O arquivo não existe', E_USER_ERROR);
   
    
}
    echo 'Ops, o programa continuou...';
