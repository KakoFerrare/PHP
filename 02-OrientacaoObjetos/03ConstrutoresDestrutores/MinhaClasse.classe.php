<?php

class MinhaClasse {

    public $nome;
    
//CONSTRUTOR É SEMPRE FEITO QUANDO EU QUERO OBRIGAR UM COMPORTAMENTO INICIAL COM PARÂMETRO
    function __construct($nome) {
        $this->nome = $nome;
        
    }
    
//É BOM SEMPRE UTILIZAR QUANDO ESTIVER CONEXÃO COM O BANCO PARA DESTRUIR OU DESCONECTAR COM O BANCO
    function __destruct() {
        echo "destrutor feito";
    }
    
}

$user = new MinhaClasse("Kako");
echo $user->nome;

$marcos = new MinhaClasse("Marcos");
echo $marcos->nome;