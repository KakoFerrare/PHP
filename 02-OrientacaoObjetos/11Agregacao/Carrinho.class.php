<?php

class Carrinho {
    
    private $nome;
    private $produtos = array();

    public function __construct($nome) {
        $this->nome = $nome;
    }
    
    public function addProdCart(Produto $produto){
        $this->produtos[] = $produto;
    }
    
    public function totalCart(){
        return count($this->produtos);
    }


    public function listProdCart(){
        echo"<h1> {$this->nome}</h1><br>";
        foreach ($this->produtos as $produto){
            echo "Nome: {$produto->getNome()} | Preço: {$produto->getPreco()} <br>";
        }
    }
    
    public function totalPrecoCart(){
        $somaProdutos = 0;
        foreach ($this->produtos as $produto){
            $somaProdutos += $produto->getPreco();
        }
        return $somaProdutos;
    }
    
}
