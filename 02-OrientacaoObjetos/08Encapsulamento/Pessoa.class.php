<?php

//ENCAPSULAMENTO, FORMA DE PROTEGER O ACESSO DOS SEUS ATRIBUTOS;

class Pessoa {

    protected $nome;
    protected $idade;
   
    
    
    
/*    public function exibeDados(){
        echo "Nome: {$this->nome} - Idade {$this->idade}";
    }
 * 
 */
    
    public function  setNome($nome){
        if(strlen($nome)>2){
            $this->nome = $nome;
            return true;
        }else{
            return false;
        }
        
    }
    
    public function  setIdade($idade){
        if($idade > 0){
        $this->idade = $idade;
        return true;
        }else{
            $this->idade = 18;
            return false;
        }
    }
    
    public function getNome(){
        return $this->nome;
    }
    
    public function getIdade(){
        return $this->idade;
    }

}

//$kako = new Pessoa();
//$kako->setNome("Kako");
//
//if($kako->setNome("Kako")){
//    echo $kako->getNome()."<br>";
//}else{
//    echo 'Nome deve conter mais que 2 caracteres';
//}
//
//
//
//if($kako->setIdade(29)){
//    echo $kako->getIdade();
//}else{
//    echo 'Idade não pode ser menor que 1';
//}
