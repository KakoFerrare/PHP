<?php

class Cliente {

    private $nome;
    private $telefone;
    private $carrinhoCompras;


    public function __construct() {
        $this->carrinhoCompras = new Carrinho('Carrinho Compras Americanas');
    }
    public function addProdCart($produto){
        $this->carrinhoCompras->addProdCart($produto);
    }
    public function getDadosCart(){
        return $this->carrinhoCompras;
    }

        public function getNome(){
        return $this->nome;
    }
    
    public function getTelefone(){
        return $this->telefone;
    }
    
    public function setNome($nome){
        $this->nome = $nome;
    }
    
    public function setTelefone($telefone){
        $this->telefone= $telefone;
    }
}
