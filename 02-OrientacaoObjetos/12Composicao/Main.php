<?php

require './Cliente.class.php';
require './Carrinho.class.php';
require './Produto.class.php';

$cliente = new Cliente();
$cliente->setNome('Kako');
$cliente->setTelefone(1936333673);

$cliente->getNome();
$cliente->getTelefone();

$sabao = new Produto();
$sabao->setNome("Sabão");
$sabao->setPreco(2.50);



$detergente = new Produto();
$detergente->setNome("Detergente");
$detergente->setPreco(10);


$bolacha = new Produto();
$bolacha->setNome("Bolacha");
$bolacha->setPreco(23.50);


$arroz = new Produto();
$arroz->setNome("Arro");
$arroz->setPreco(8.50);

$cliente->addProdCart($sabao);
$cliente->addProdCart($detergente);
$cliente->addProdCart($bolacha);
$cliente->addProdCart($arroz);

$cliente->getDadosCart()->listProdCart();