<?php


class Pessoa {

    private $nome;
    private $telefone;
    
    //MÉTODOS MAGICOS OU INTERCEPTADORES
    //ELES GERAM O GET E SET PARA TODAS OS ATRIBUTOS DA CLASSE
    
    public function __set($name, $value) {
        $this->$name = $value;
    }
    
    public function __get($name) {
        return $this->$name;
    }
    
    //MÉTODO CALL É INVOCADO TODA VEZ QUE UM MÉTODO DA CLASSE QUE NÃO EXISTE É INVOCADO
    //MUITO IMPORTANTE PARA TRATAMENTO DE ERROS
    //$name = NOME DO MÉTODO INVOCADO
    //$arguments = PARAMETROS DO MÉTODO
    public function __call($name, $arguments) {
        $parametros = implode(' , ', $arguments);
        echo "O método {$name} não existe -> {$parametros}";
    }
}

$kako = new Pessoa();
$kako->nome = 'Kako';
$kako->telefone = 36333673;

echo $kako->nome;
echo "<br><hr>";
$kako->mostrarDados('as');