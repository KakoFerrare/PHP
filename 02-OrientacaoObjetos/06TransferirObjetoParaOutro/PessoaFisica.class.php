<?php
require './Pessoa.class.php';

//SE A MINHA CLASSE NÃO PUDER SER EXTENDIDA, OUTRA CLASSE HERDA-LA EU PASSA O FINAL ANTES DO CLASS
//SETANDO QUE ESTA CLASSE NÃO PODE PASSAR SUA HERANÇA PARA NENHUM OBJETO
final class PessoaFisica extends Pessoa{

    public $cpf;
    
    function exibeDoc() {
        return $this->cpf;
    }
}

$marcos = new PessoaFisica();
$marcos->nome = 'Marcos';
$marcos->telefone = '3633 36 73';
$marcos->email = 'marcos@hotmail.com';
$marcos->cpf = '123.456.344.45';
$marcos->totalDinheiro = 200;

$kako = new PessoaFisica();
$kako->nome = 'Kako';
$kako->telefone = '9934595';
$kako->email = 'kako@hotmail.com';
$kako->cpf = '123.456.344.45';
$kako->totalDinheiro = 300;



$marcos->exibeSaldo();
echo '<br>';
$kako->exibeSaldo();

$marcos->transfereSaldo($marcos,$kako, 500);
echo'<br><br>';

$marcos->exibeSaldo();
echo '<br>';
$kako->exibeSaldo();