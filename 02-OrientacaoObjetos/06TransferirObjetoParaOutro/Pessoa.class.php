<?php


//ABSTRAÇÃO É ISOLAR A CLASSE DE PESSOA, NO CASO COMO PESSOA VAI SER ABSTRATA, O NOVO OBJETO
//SÓ PODE SER FISICA OU JURIDICA, NUNCA SÓ DE PESSOA
abstract class Pessoa {

    public $nome;
    public $telefone;
    public $email;
    public $totalDinheiro;
    
    function exibeDados(){
        
        echo "Nome: {$this->nome} - Telefone: {$this->telefone} - E-mail: {$this->email}";
    }
    
    function exibeSaldo(){
        
        echo "Nome: {$this->nome} - Saldo: {$this->totalDinheiro}";
    }
    
    function transfereSaldo(Pessoa $de, Pessoa $para, $valor){
        if($valor <= $de->totalDinheiro){
        $de->totalDinheiro -= $valor;
        $para->totalDinheiro += $valor;
        }else{
            echo 'O valor é superior ao saldo';
        }
    }
   

    
    
    //OBRIGO AS CLASSES QUE TENHAM HERANÇA, QUE ESCREVAM O MÉTODO EXIBEDOC
    abstract function exibeDoc();
    
    //SE EU QUISER DIZER QUE ESTE MÉTODO NÃO PODE SER REESCRITO EU TAMBEM POSSO PASSAR O FINAL
    final function exibeNomeTelefone(){
        echo "Nome: {$this->nome} - Telefone: {$this->telefone}";
    }
    
}

//QUANDO FAÇO ABSTRAÇÃO EU NÃO POSSO MAIS ESTANCIAR A CLASSE
//$teste = new Pessoa();
//$teste->nome = "Pessoa teste";
//$teste->telefone = "Telefone teste";
//$teste->email = "E-mail teste";
//$teste->exibeDados();