<?php

require './ClassMae.class.php';

class ClassExemplo extends ClassMae{
    //CONSTANTE E ESTATICO SEMPRE PERTENCEM A CLASSE NÃO AO OBJETO
    const idade  = 20;
    static $totalObjeto = 0;
    //CONSTANTE E ESTATICO SEMPRE PERTENCEM A CLASSE NÃO AO OBJETO
    
    private $outroNome;
    
    static function exibeDados(){
        return 'Idade: '.self::idade.', Nome: ' .parent::nome;
        //FORMA DE ACESSAR A CONSTANTE DESTA CLASSE E A DA CLASSE HERDADA
    }
    
    public function setNome($nome){
        $this->outroNome = $nome;
        self::$totalObjeto++;
    }
    
    public function getNome(){
        return '<br>'.$this->outroNome;
    }
}

echo ClassMae::nome;

$obj = new ClassExemplo();
$obj->setNome("Kako");
echo $obj->getNome();
echo '<br>';
echo ClassExemplo::$totalObjeto;
echo '<br>';
//MÉTODOS ESTATICOS NÃO PRECISAM SER CHAMADO PELO OBJETO, MAS SIM SÓ PELA CLASSE
echo ClassExemplo::exibeDados();