<?php


class ClassMae {
    //CONSTANTES NÃO PRECISÃO DE MÉTODOS PARA SEREM ACESSADOS
    const nome = 'nome da constante';
    public $nome2;
    
    //FORMA DE RETORNAR VALOR DA CONSTANTE POR FUNÇÃO
//    public function exibeDados(){
//        return self::nome;
//    }
}

//PARA ACESSAR CONSTANTES USO A SEGUINTE NOMENCLATURA, NÃO PRECISO ESTANCIAR POIS É UMA PROPRIEDADE DA CLASSE
ClassMae::nome;

//PARA ACESSAR UM ATRIBUTO DA CLASSE
$novo = new ClassMae();
$novo->nome2;