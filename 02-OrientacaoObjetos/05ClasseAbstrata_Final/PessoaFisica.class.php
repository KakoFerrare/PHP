<?php
require './Pessoa.class.php';

//SE A MINHA CLASSE NÃO PUDER SER EXTENDIDA, OUTRA CLASSE HERDA-LA EU PASSA O FINAL ANTES DO CLASS
//SETANDO QUE ESTA CLASSE NÃO PODE PASSAR SUA HERANÇA PARA NENHUM OBJETO
final class PessoaFisica extends Pessoa{

    public $cpf;
    
    function exibeDoc() {
        return $this->cpf;
    }
}

$marcos = new PessoaFisica();
$marcos->nome = 'Marcos';
$marcos->telefone = '3633 36 73';
$marcos->email = 'kako@hotmail.com';
$marcos->cpf = '123.456.344.45';


$marcos->exibeDados();
$marcos->exibeNomeTelefone();