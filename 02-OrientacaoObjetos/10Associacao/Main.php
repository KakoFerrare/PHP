<?php

require './Loja.class.php';
require './Cliente.class.php';

$cliente = new Cliente();
$cliente->setNome("Kako");
$cliente->setTelefone("3633 36 73");

$loja = new Loja();
$loja->setNome("Americanas");
$loja->setCnpj("1112.11122.3.4.54");

//ASSOCIAÇÃO
$loja->Cliente = $cliente;

echo $loja->Cliente->getNome();
