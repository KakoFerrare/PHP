<?php
require './EscolaInterface.php';
//IMPLEMENTS DIZ QUE USA UMA INTERFACE (ESCOLAINTERFACE) E ME OBRIGA A IMPLEMENTAR OS 2 MÉTODOS DA INTERFACE

//SE EU TIVER 2 INTERFACES:
//class EscolaPublica implements EscolaInterface, NovaInterface {

class EscolaPublica implements EscolaInterface {
    
    public function registraAlvaraPrefeitura() {
        echo 'Método para registrar alvara<br>';
    }

    public function validarExtintores() {
        echo 'Método para validar extintores<br>';
    }

    public function validaTamanhoRecepcao() {
        
    }

}

$drCristianoOsorio = new EscolaPublica();
$drCristianoOsorio->registraAlvaraPrefeitura();
$drCristianoOsorio->validarExtintores();