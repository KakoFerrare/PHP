<?php

//NA INTERFACE EU DEFINO QUE TODO OBJETO QUE HERDAR ESCOLAINTERFACE, DEVE OU SÃO OBRIGADAS
//A TEREM OS MÉTODOS DECLARADOS NA CLASSE DE INTERFACE

//OBS: NA CLASSE INTERFACE VOCÊ APENAS DECLARA A ASSINATURA DO MÉTODO:
// function registraAlvaraPrefeitura();
//O MÉTODO NÃO DEVE TER CORPO, POIS NA CLASSES HERDADAS É QUE ESCREVEMOS O MÉTODO
interface EscolaInterface {

    function registraAlvaraPrefeitura();
    
    function validarExtintores();

    function validaTamanhoRecepcao();
}
