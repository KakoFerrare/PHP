<?php

$telefone = 3454353453;

//RETORNA TRUE OU FALSE, SE A VARIAVEL É INTEIRO
is_int($telefone);
//RETORNA O TIPO DA VARIAVEL
gettype($telefone);

//RETORNA TRUE OU FALSE, SE A VARIAVEL É STRING
is_string($telefone);

//SE A VARIAVVEL É NULA
is_null($telefone);

//VERIFICA SE A VARIAVEL EXISTE
isset($telefone);

//VERIFICA SE A VARIAVEL ESTÁ VAZIA
empty($telefone);