<?php
/*
if(!file_exists('file2.txt')){
    $e = new Exception('file not found', E_USER_WARNING);
    
    echo $e->getCode();
    echo '<br><br>';
    
    echo $e->getFile();
    echo '<br><br>';
    
    echo $e->getLine();
    echo '<br><br>';
    
    echo $e->getMessage();
    echo '<br><br>';
    
    echo $e->getPrevious();
    echo '<br><br>';
    
    var_dump($e->getTrace());
    echo '<br><br>';
    
    echo $e->getTraceAsString();
    echo '<br><br>';
}
*/

//MESMA COISA
try {
    if(!file_exists('file2.txt')){
        //QUANDO LANÇO UM THROW ASSIM QUE ELE ENTRA NELE, ELE JA CAI O CATCH
        //REPARE QUE O ECHO NADA NÃO FOI EXECUTADO
        throw new Exception('Arquivo não encontrado', E_USER_NOTICE);
        echo 'Nada';
    }
} catch (Exception $ex) {
    echo $ex->getMessage();
}
