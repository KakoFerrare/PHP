<?php

class Obra {
    
    public $nomeObra;
    public $tamanhoLote;
    public $altura;
    public $largura;
    
    function AumentarAltura($altura){
        $this->altura += $altura;
    }
    
    function AumentarLargura($largura){
        $this->largura += $largura;
    }
    
    function MostarDadosObra(){
        echo "Nome: {$this->nomeObra}<br>Tamanho do lote: {$this->tamanhoLote}<br>Altura: {$this->altura}<br>Largura: {$this->largura}";
    }
}


