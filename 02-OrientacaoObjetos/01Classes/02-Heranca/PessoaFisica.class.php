<?php
require './Pessoa.class.php';

class PessoaFisica extends Pessoa{

    public $cpf;
    
}

$marcos = new PessoaFisica();
$marcos->nome = 'Marcos';
$marcos->telefone = '3633 36 73';
$marcos->email = 'kako@hotmail.com';
$marcos->cpf = '123.456.344.45';
