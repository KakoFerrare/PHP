<?php
require './Pessoa.class.php';

class PessoaJuridica extends Pessoa{

    public $cnpj;
    public $razaoSocial;
    
    
}


$empresa = new PessoaJuridica();
$empresa->nome = 'MINHA EMPRESA';
$empresa->telefone = '3633 36 73';
$empresa->email = 'empresa@hotmail.com';
$empresa->cnpj= '1223.43456.34244.23345';
$empresa->razaoSocial= 'EMPRESA';
