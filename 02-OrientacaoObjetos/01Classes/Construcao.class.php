<?php


class Construcao {
    
    public $nomeResponsavel;
    public $tijolos;
    public $cimento;
    public $tamanhoLote;
    
    function mostraDadosConstrucao(){
        echo "A casa do {$this->nomeResponsavel} precisa de {$this->tijolos} tijolos e {$this->cimento} sacos de cimento e o lote de {$this->tamanhoLote}";
    }
    
}

$casaKako = new Construcao();
$casaKako->nomeResponsavel = 'Kako';
$casaKako->tijolos = 1000;
$casaKako->cimento = 35;
$casaKako->tamanhoLote = '10000 metros';

$casaKako->mostraDadosConstrucao();

